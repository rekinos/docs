### All Packages
In general, changes will not be mentioned for:
 - Placing files into /usr, rather than / or /usr/local
 - Merging /usr/lib64 and /usr/lib
 - Merging /usr/sbin and /usr/bin
 - Adding initscripts and alternatives
 - Enabling additional build artifacts

### cpio
Removed rmt from package, we reference the one from tar instead

### findutils
Applied patches to fix build with glibc 2.28. Should be unnecessary after next update

### gcc
Multilib is disabled for now, as we do not currently support multilib

### gdk-pixbuf
Added alpm hook to run gdk-pixbuf-query-loaders

### grub
Applied patches to fix build with latest GCC and binutils

### libgpg-error
Applied patches to fix build with gawk 5

### m4
Applied patches to fix build with glibc 2.28

#### make
Applied patches to fix build with glibc 2.28

#### man-db
- Removed systemd files, as we don't use systemd
- Used root as cache owner, disabled setuid (we don't really have a system for system users yet, and this is what Arch does)

### ncurses
"redirect non-widec to widec through dark magic I don't understand"

Added links to provide libcurses

### netsurf
Applied patch to fix rendering where DPI is undefined (encountered in weston)

### openssl
Added suffix to manpages to avoid conflicts (with what? I don't remember)

### readline
Added ncurses to SHLIB_LIBS ("I don't know what this does, I just copied it from Artix")

### runit
Changed install location to standard paths. (notable because runit usually uses really weird ones)

### shadow
Disabled SELinux integration (maybe this should come back?)

Added extra PAM config files

### sudo
Enabled reading EDITOR for visudo

Added PAM config

### sway
Added setuid to sway binary for use without logind

### tar
Moved rmt to /usr/bin for use by other packages
