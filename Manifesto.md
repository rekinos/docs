# Rekin aims to be:
## 1. Free
All relevant parts of the project should be released under a Free Software
license, and distributed without cost to all who wish to acquire it.

For the official package repositories, this means that no non-free packages will
be offered.

## 2. Flexible
Rekin should be open to components being chosen and replaced. This means that
programs and packages should not introduce unnecessary coupling to specific
dependencies when a more general requirement is sufficient. In the case of
programs with multiple implementations, it should be possible for users to
select which is used by default.

Libraries should be dynamically linked where practical, and projects which
cannot support this may not be accepted into the official package repositories.

Packages should also be offered with a selection of build options where
applicable.

## 3. Reliable
Whenever possible, practices should be adopted to ensure functionality and
quality of components and packages are maintained. This may include efforts
such as linting and automated testing, as well as early access to updates for
user testing.

Packaging should strive to create reproducible builds, so that package
integrity can be independently verified.

## 4. Up-to-Date
An effort should be made to ensure timely updates from upstream sources are
available to users, but not at the expense of reliability. Package updates
should be tested against all other relevant packages to detect and avoid
breakage.
